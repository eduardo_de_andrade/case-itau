package br.com.itau.domain;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pessoa_fisica")
public class PessoaFisica {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(length = 20)
	private int cd_pess_pf;
	@Column(length = 11)
	private String num_cpf;

	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinTable(name = "conta_pessoapf ", joinColumns = @JoinColumn(name = "cod_pess"), inverseJoinColumns = @JoinColumn(name = "cd_pess_pf"))
	private Collection<Conta> listContas;

	public PessoaFisica() {
	}

	public int getCd_pess_pf() {
		return cd_pess_pf;
	}

	public void setCd_pess_pf(int cd_pess_pf) {
		this.cd_pess_pf = cd_pess_pf;
	}

	public String getNum_cpf() {
		return num_cpf;
	}

	public void setNum_cpf(String num_cpf) {
		this.num_cpf = num_cpf;
	}

	public Collection<Conta> getListContas() {
		return listContas;
	}

	public void setListContas(Collection<Conta> listContas) {
		this.listContas = listContas;
	}
	
}
