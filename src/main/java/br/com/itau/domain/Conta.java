package br.com.itau.domain;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "conta")
public class Conta {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(length = 20)
	private int cod_unico_conta;

	@Column(length = 10)
	private String num_conta;

	@Column(length = 1)
	private char dig_conta;
	@Column(length = 5)
	private String num_agencia;
	@Column(length = 1)
	private char tipo_conta;
	@Column(length = 20)
	private int cd_pess;
	
	
	public Conta() {
	}


	public int getCod_unico_conta() {
		return cod_unico_conta;
	}


	public void setCod_unico_conta(int cod_unico_conta) {
		this.cod_unico_conta = cod_unico_conta;
	}


	public String getNum_conta() {
		return num_conta;
	}


	public void setNum_conta(String num_conta) {
		this.num_conta = num_conta;
	}


	public char getDig_conta() {
		return dig_conta;
	}


	public void setDig_conta(char dig_conta) {
		this.dig_conta = dig_conta;
	}


	public String getNum_agencia() {
		return num_agencia;
	}


	public void setNum_agencia(String num_agencia) {
		this.num_agencia = num_agencia;
	}


	public char getTipo_conta() {
		return tipo_conta;
	}


	public void setTipo_conta(char tipo_conta) {
		this.tipo_conta = tipo_conta;
	}


	public int getCd_pess() {
		return cd_pess;
	}


	public void setCd_pess(int cd_pess) {
		this.cd_pess = cd_pess;
	}
	
	

}
