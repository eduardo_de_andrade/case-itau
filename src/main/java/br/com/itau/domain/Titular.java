package br.com.itau.domain;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "titular")
public class Titular {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(length = 20)
	private int cod_titular;

	@Column(length = 20)
	private int cod_unico_conta;

	@Column(length = 20)
	private int cd_pess;
	@Column(length = 2)
	private String tipo_titular;

	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinTable(name = "pessoapj_titular ", joinColumns = @JoinColumn(name = "cod_titular"), inverseJoinColumns = @JoinColumn(name = "cod_pess_pj"))
	private Collection<PessoaJuridica> listPJ;
	
	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinTable(name = "pessoapf_titular ", joinColumns = @JoinColumn(name = "cod_titular"), inverseJoinColumns = @JoinColumn(name = "cod_pess_pf"))
	private Collection<PessoaFisica> listPf;

	public Titular() {
	}

	public int getCod_titular() {
		return cod_titular;
	}

	public void setCod_titular(int cod_titular) {
		this.cod_titular = cod_titular;
	}

	public int getCod_unico_conta() {
		return cod_unico_conta;
	}

	public void setCod_unico_conta(int cod_unico_conta) {
		this.cod_unico_conta = cod_unico_conta;
	}

	public int getCd_pess() {
		return cd_pess;
	}

	public void setCd_pess(int cd_pess) {
		this.cd_pess = cd_pess;
	}

	public String getTipo_titular() {
		return tipo_titular;
	}

	public void setTipo_titular(String tipo_titular) {
		this.tipo_titular = tipo_titular;
	}

	public Collection<PessoaJuridica> getListPJ() {
		return listPJ;
	}

	public void setListPJ(Collection<PessoaJuridica> listPJ) {
		this.listPJ = listPJ;
	}

	public Collection<PessoaFisica> getListPf() {
		return listPf;
	}

	public void setListPf(Collection<PessoaFisica> listPf) {
		this.listPf = listPf;
	}
	

}
