package br.com.itau.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "favorecido")
public class Favorecido {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(length = 20)
	private int cd_favorecido;
	@Column(length = 10)
	private String num_conta;
	@Column(length = 1)
	private char dig_conta;
	@Column(length = 5)
	private String num_agencia;
	@Column(length = 1)
	private char tipo_conta;
	@Column(length = 2)
	private String tipo_documento;
	@Column(length = 3)
	private int tipo_banco;
	@Column(length = 14)
	private String num_documento;


	public Favorecido() {
	}


	public int getCd_favorecido() {
		return cd_favorecido;
	}


	public void setCd_favorecido(int cd_favorecido) {
		this.cd_favorecido = cd_favorecido;
	}


	public String getNum_conta() {
		return num_conta;
	}


	public void setNum_conta(String num_conta) {
		this.num_conta = num_conta;
	}


	public char getDig_conta() {
		return dig_conta;
	}


	public void setDig_conta(char dig_conta) {
		this.dig_conta = dig_conta;
	}


	public String getNum_agencia() {
		return num_agencia;
	}


	public void setNum_agencia(String num_agencia) {
		this.num_agencia = num_agencia;
	}


	public char getTipo_conta() {
		return tipo_conta;
	}


	public void setTipo_conta(char tipo_conta) {
		this.tipo_conta = tipo_conta;
	}


	public String getTipo_documento() {
		return tipo_documento;
	}


	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}


	public int getTipo_banco() {
		return tipo_banco;
	}


	public void setTipo_banco(int tipo_banco) {
		this.tipo_banco = tipo_banco;
	}


	public String getNum_documento() {
		return num_documento;
	}


	public void setNum_documento(String num_documento) {
		this.num_documento = num_documento;
	}
	
}
