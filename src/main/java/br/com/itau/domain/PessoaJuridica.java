package br.com.itau.domain;

import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pessoa_juridica")
public class PessoaJuridica {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(length = 20)
	private int cod_pess_pj;

	@Column(length = 14)
	private String num_cnpj;
	
	@ManyToMany(cascade = { CascadeType.ALL }, fetch = FetchType.LAZY)
	@JoinTable(name = "conta_pessoapj ", joinColumns = @JoinColumn(name = "cod_pess_pj"), inverseJoinColumns = @JoinColumn(name = "cd_pess"))
	private Collection<Conta> listContas;
	
	public PessoaJuridica() {
	}

	public int getCod_pess_pj() {
		return cod_pess_pj;
	}

	public void setCod_pess_pj(int cod_pess_pj) {
		this.cod_pess_pj = cod_pess_pj;
	}

	public String getNum_cnpj() {
		return num_cnpj;
	}

	public void setNum_cnpj(String num_cnpj) {
		this.num_cnpj = num_cnpj;
	}

	public Collection<Conta> getListContas() {
		return listContas;
	}

	public void setListContas(Collection<Conta> listContas) {
		this.listContas = listContas;
	}
	
	

}
