package br.com.itau.mock;

import java.util.ArrayList;
import java.util.List;

import br.com.itau.domain.Favorecido;

public class FavorecidosMock {

	public static List<String> listContas;
	public static List<String> listAgencias;
	public static List<String> listCnpj;
	public static List<String> listCpf;
	public static List<String> listBanco;
	public static List<Favorecido> listFavorecido;

	public static List<String> listCContas() {
		listContas = new ArrayList<String>();

		listContas.add("12433");
		listContas.add("23245");
		listContas.add("25436");
		listContas.add("78535");

		return listContas;
	}

	public static List<String> listAagencia() {
		listAgencias = new ArrayList<String>();

		listAgencias.add("123");
		listAgencias.add("235");
		listAgencias.add("256");
		listAgencias.add("785");

		return listAgencias;
	}

	public static List<String> listCcnpj() {
		listCnpj = new ArrayList<String>();

		listCnpj.add("21.256.021/0001-00");
		listCnpj.add("41.156.021/0002-20");
		listCnpj.add("46.226.023/0003-10");
		listCnpj.add("87.178.065/0001-30");
		return listCnpj;
	}

	public static List<String> listCCpf() {
		listCpf = new ArrayList<String>();
		listCpf.add("286.536.658-12");
		listCpf.add("854.021.256-19");
		listCpf.add("895.025.213-65");
		listCpf.add("102.265.524-54");
		return listCpf;
	}

	public static List<String> listBbanco() {
		listBanco = new ArrayList<String>();
		listBanco.add("341");
		listBanco.add("025");
		listBanco.add("365");
		listBanco.add("215");
		return listBanco;
	}
}
