package br.com.itau.service;

import java.util.List;
import java.util.Map;

import org.springframework.validation.annotation.Validated;

import br.com.itau.domain.Favorecido;

public interface FavorecidoService {
	
	public List<Favorecido> buscaFavorecidosCpf(@Validated String documento);
	public List<Favorecido> buscaFavorecidosCnpj(@Validated String documento);
	public Map<String,List<Favorecido>> contasPjPf(@Validated String cpfOrCnpj);

}
