package br.com.itau.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.domain.Favorecido;
import br.com.itau.service.FavorecidoService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController()
@RequestMapping("/api/favoritos")
public class BuscaFavoritosCnpj {

	@Autowired
	private FavorecidoService service;
	
	@GetMapping("/cnpj")
	public ResponseEntity<?> searchFavoditos(@RequestParam("cnpj") String cnpj) {

		List<Favorecido>listFavorecido = service.buscaFavorecidosCnpj(cnpj);
		
		return ResponseEntity.ok(listFavorecido);
	}
}
