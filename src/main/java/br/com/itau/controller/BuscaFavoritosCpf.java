package br.com.itau.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.domain.Favorecido;
import br.com.itau.mock.FavorecidosMock;
import br.com.itau.service.FavorecidoService;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController()
@RequestMapping("/api/favoritos")
public class BuscaFavoritosCpf {

	@Autowired
	private FavorecidoService service;
	
	@GetMapping("/cpf")
	public ResponseEntity<?> searchFavoditos(@RequestParam("cpf") String cpf) {

		List<Favorecido>listFavorecido = service.buscaFavorecidosCpf(cpf);
		
		return ResponseEntity.ok(listFavorecido);
	}
}
