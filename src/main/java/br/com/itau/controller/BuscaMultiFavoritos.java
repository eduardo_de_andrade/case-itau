package br.com.itau.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.domain.Favorecido;
import br.com.itau.mock.FavorecidosMock;
import br.com.itau.service.FavorecidoService;
import br.com.itau.serviceImpl.FavorecidoServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController()
@RequestMapping("/api/favoritos")
public class BuscaMultiFavoritos {
	
	@Autowired
	private FavorecidoServiceImpl service;
	
	@GetMapping("/cpf-cnpj")
	public ResponseEntity<?> searchFavoditos(@RequestParam("documento") String documento) {

		Map<String,List<Favorecido>> listMultiContas = service.contasPjPf(documento);
		
		return ResponseEntity.ok(listMultiContas);
	}
}
