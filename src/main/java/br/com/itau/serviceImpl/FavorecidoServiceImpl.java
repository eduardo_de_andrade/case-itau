package br.com.itau.serviceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import br.com.itau.domain.Favorecido;
import br.com.itau.mock.FavorecidosMock;
import br.com.itau.service.FavorecidoService;

@Service
public class FavorecidoServiceImpl implements FavorecidoService {

	public static List<String> listContas;
	public static List<String> listAgencias;
	public static List<String> listCnpj;
	public static List<String> listCpf;
	public static List<String> listBanco;
	public static List<Favorecido> listFavorecido;
	public FavorecidosMock mock;

	@Override
	public List<Favorecido> buscaFavorecidosCpf(String documento) {
		Favorecido favorecido;
		listFavorecido = new ArrayList<Favorecido>();

		for (int i = 0; i < mock.listCCpf().size(); i++) {
			favorecido = new Favorecido();

			if (mock.listCCpf().get(i).contains(documento)) {

				favorecido.setCd_favorecido(i);
				favorecido.setDig_conta((char) i);
				favorecido.setNum_agencia(mock.listAagencia().get(i));
				favorecido.setNum_conta(mock.listCContas().get(i));
				favorecido.setNum_documento(mock.listCCpf().get(i));
				favorecido.setTipo_banco(Integer.valueOf(mock.listBbanco().get(i)));
				favorecido.setTipo_conta('1');
				favorecido.setTipo_documento("PF");
				listFavorecido.add(favorecido);

			}

		}
		return listFavorecido;

	}

	@Override
	public List<Favorecido> buscaFavorecidosCnpj(String documento) {
		Favorecido favorecido;

		listFavorecido = new ArrayList<Favorecido>();
		for (int i = 0; i < mock.listCcnpj().size(); i++) {
			favorecido = new Favorecido();

			if (mock.listCcnpj().get(i).contains(documento)) {

				favorecido.setCd_favorecido(i);
				favorecido.setDig_conta((char) i);
				favorecido.setNum_agencia(mock.listAagencia().get(i));
				favorecido.setNum_conta(mock.listCContas().get(i));
				favorecido.setNum_documento(mock.listCcnpj().get(i));
				favorecido.setTipo_banco(Integer.valueOf(mock.listBbanco().get(i)));
				favorecido.setTipo_conta('2');
				favorecido.setTipo_documento("PJ");
				listFavorecido.add(favorecido);

			}

		}
		return listFavorecido;

	}

	@Override
	public Map<String, List<Favorecido>> contasPjPf(String cpfOrCnpj) {
		Favorecido favorecido;
		Map<String, List<Favorecido>> favMultConta = new HashMap<String, List<Favorecido>>();
		listFavorecido = new ArrayList<Favorecido>();
		for (int i = 0; i < 2; i++) {

			favorecido = new Favorecido();
			if (mock.listCCpf().get(i).contains(cpfOrCnpj) || mock.listCcnpj().get(i).contains(cpfOrCnpj)) {

				favorecido.setCd_favorecido(i);
				favorecido.setDig_conta((char) i);
				favorecido.setNum_agencia(mock.listAagencia().get(i));
				favorecido.setNum_conta(mock.listCContas().get(i));
				favorecido.setNum_documento(mock.listCCpf().get(i));
				favorecido.setTipo_banco(Integer.valueOf(mock.listBbanco().get(i)));
				favorecido.setTipo_conta('1');
				favorecido.setTipo_documento("PF");

				listFavorecido.add(favorecido);

				for (int cont = 1; cont < 3; cont++) {

					favorecido = new Favorecido();
					favorecido.setCd_favorecido(cont);
					favorecido.setDig_conta((char) cont);
					favorecido.setNum_agencia(mock.listAagencia().get(cont));
					favorecido.setNum_conta(mock.listCContas().get(cont));
					favorecido.setNum_documento(mock.listCcnpj().get(cont));
					favorecido.setTipo_banco(Integer.valueOf(mock.listBbanco().get(cont)));
					favorecido.setTipo_conta('2');
					favorecido.setTipo_documento("PJ");
					listFavorecido.add(favorecido);
				}
				favMultConta.put(cpfOrCnpj, listFavorecido);
				break;
			}
		}
		return favMultConta;
	}
}