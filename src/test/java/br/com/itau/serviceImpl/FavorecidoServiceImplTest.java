package br.com.itau.serviceImpl;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.itau.domain.Favorecido;

@ExtendWith(MockitoExtension.class)
class FavorecidoServiceImplTest {

	/*
	 * @Bean public FavorecidoService favorecidoService() { return new
	 * FavorecidoServiceImpl(); }
	 */
	@InjectMocks
	public FavorecidoServiceImpl service;

	@Mock
	List<Favorecido> listFavCnpj;

	@BeforeEach
	void setUp() throws Exception {

		Favorecido fv = new Favorecido();
		fv.setCd_favorecido(1);
		listFavCnpj = new ArrayList<Favorecido>();
		listFavCnpj.add(fv);

	}
	@Test
	void test() {
		assertNotNull(service.buscaFavorecidosCnpj("21.256.021/0001-00"));
		assertNotNull(service.buscaFavorecidosCpf("854.021.256-19"));
		assertNotNull(service.contasPjPf("854.021.256-19"));
	}

}
