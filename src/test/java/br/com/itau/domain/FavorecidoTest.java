package br.com.itau.domain;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FavorecidoTest {

	Favorecido favorecido;

	@BeforeEach
	void setUp() throws Exception {
		favorecido = new Favorecido();
		favorecido.setCd_favorecido(1);
		favorecido.setDig_conta('1');
		favorecido.setNum_agencia("123");
		favorecido.setNum_conta("1234");
		favorecido.setNum_documento("12344");
		favorecido.setTipo_banco(1);
		favorecido.setTipo_conta('2');
		favorecido.setTipo_documento("1");
	}

	@Test
	void test() {
		assertNotNull(favorecido);
		assertNotNull(favorecido.getCd_favorecido());
		assertNotNull(favorecido.getDig_conta());
		assertNotNull(favorecido.getNum_agencia());
		assertNotNull(favorecido.getNum_conta());
		assertNotNull(favorecido.getNum_documento());
		assertNotNull(favorecido.getTipo_banco());
		assertNotNull(favorecido.getTipo_conta());
		assertNotNull(favorecido.getTipo_documento());
	}

}
