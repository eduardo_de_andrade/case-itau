package br.com.itau.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PessoaFisicaTest {

	PessoaFisica pf;

	@BeforeEach
	void setUp() throws Exception {
		Collection<Conta> listContas = new ArrayList<Conta>();

		Conta conta = new Conta();
		conta.setCd_pess(1);
		conta.setCod_unico_conta(1232);
		conta.setDig_conta('1');
		conta.setNum_agencia("1232312");
		conta.setNum_conta("342342");
		conta.setTipo_conta('1');

		listContas.add(conta);

		pf = new PessoaFisica();
		pf.setCd_pess_pf(1);
		pf.setListContas(listContas);
		pf.setNum_cpf("1234");
	}
	
	@Test
	public void testFields() {
	assertNotNull(pf);
	assertNotNull(pf.getCd_pess_pf());
	assertNotNull(pf.getListContas());
	assertNotNull(pf.getNum_cpf());
	assertEquals("1234", pf.getNum_cpf());
	}

}
