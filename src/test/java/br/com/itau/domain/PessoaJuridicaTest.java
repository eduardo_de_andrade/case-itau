package br.com.itau.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PessoaJuridicaTest {

	PessoaJuridica pj;

	@BeforeEach
	void setUp() throws Exception {
		Collection<Conta> listContas = new ArrayList<Conta>();

		Conta conta = new Conta();
		conta.setCd_pess(1);
		conta.setCod_unico_conta(1232);
		conta.setDig_conta('1');
		conta.setNum_agencia("1232312");
		conta.setNum_conta("342342");
		conta.setTipo_conta('1');

		listContas.add(conta);

		pj = new PessoaJuridica();
		pj.setCod_pess_pj(1);
		pj.setListContas(listContas);
		pj.setNum_cnpj("1234");
		
	}

	@Test
	void test() {
		assertNotNull(pj);
		assertNotNull(pj.getCod_pess_pj());
		assertNotNull(pj.getListContas());
		assertNotNull(pj.getNum_cnpj());
		assertEquals("1234", pj.getNum_cnpj());
	}

}
