package br.com.itau.domain;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ContaTest {

	Conta conta;
	
	@BeforeEach
	void setUp() throws Exception {
		conta = new Conta();
		conta.setCd_pess(1);
		conta.setCod_unico_conta(1);
		conta.setDig_conta('1');
		conta.setNum_agencia("1234");
		conta.setNum_conta("1232");
		conta.setTipo_conta('1');
	}

	@Test
	void test() {
		assertNotNull(conta);
		assertNotNull(conta.getCd_pess());
		assertNotNull(conta.getCod_unico_conta());
		assertNotNull(conta.getDig_conta());
		assertNotNull(conta.getNum_agencia());
		assertNotNull(conta.getNum_conta());
		assertNotNull(conta.getTipo_conta());
	}

}
