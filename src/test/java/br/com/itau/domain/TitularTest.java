package br.com.itau.domain;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class TitularTest {

	Titular titular;
	@BeforeEach
	void setUp() throws Exception {
		Collection<PessoaFisica> listPf = new ArrayList<PessoaFisica>();
		Collection<PessoaJuridica> listPj = new ArrayList<PessoaJuridica>();
		Collection<Conta> listContas = new ArrayList<Conta>();
		
		Conta conta = new Conta();
		conta.setCd_pess(1);
		conta.setCod_unico_conta(1232);
		conta.setDig_conta('1');
		conta.setNum_agencia("1232312");
		conta.setNum_conta("342342");
		conta.setTipo_conta('1');
		
		listContas.add(conta);
		
		PessoaFisica pessoa = new PessoaFisica();
		pessoa.setCd_pess_pf(1);
		pessoa.setNum_cpf("2123123");
		pessoa.setListContas(listContas);
		listPf.add(pessoa);
	
		PessoaJuridica pessoaPJ = new PessoaJuridica();
		pessoaPJ.setCod_pess_pj(1);
		pessoaPJ.setListContas(listContas);
		pessoaPJ.setNum_cnpj("1232");
		listPj.add(pessoaPJ);
		
		titular = new Titular();
		titular.setCd_pess(1);
		titular.setCod_titular(1);
		titular.setCod_unico_conta(1);
		titular.setTipo_titular("PJ");
		
		titular.setListPf(listPf);
		
		titular.setListPJ(listPj);
		
	}

	@Test
	void test() {
		assertNotNull(titular);
		assertNotNull(titular.getCd_pess());
		assertNotNull(titular.getCod_titular());
		assertNotNull(titular.getCod_unico_conta());
		assertNotNull(titular.getListPf());
		assertNotNull(titular.getListPJ());
		assertNotNull(titular.getTipo_titular());
		
		assertNotNull(titular.getListPf());
		assertNotNull(titular.getListPJ());
		
		assertNotNull(titular.getListPf());
	}

}
