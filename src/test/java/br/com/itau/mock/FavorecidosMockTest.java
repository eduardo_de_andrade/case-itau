package br.com.itau.mock;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FavorecidosMockTest {
	
	static FavorecidosMock mock;
	List<String>listAgencias;
	List<String>listCnpj;
	List<String>listCpf;
	List<String>listBanco;
	List<String>listContas;
	
	@BeforeEach
	void setUp() throws Exception {
		listAgencias = new ArrayList<String>();
		listAgencias.add("123");
		listAgencias.add("235");
		listAgencias.add("256");
		listAgencias.add("785");
		
		listCnpj = new ArrayList<String>();

		listCnpj.add("21.256.021/0001-00");
		listCnpj.add("41.156.021/0002-20");
		listCnpj.add("46.226.023/0003-10");
		listCnpj.add("87.178.065/0001-30");
		
		listCpf = new ArrayList<String>();
		listCpf.add("286.536.658-12");
		listCpf.add("854.021.256-19");
		listCpf.add("895.025.213-65");
		listCpf.add("102.265.524-54");
		
		listBanco = new ArrayList<String>();
		listBanco.add("341");
		listBanco.add("025");
		listBanco.add("365");
		listBanco.add("215");
		
		listContas = new ArrayList<String>();

		listContas.add("12433");
		listContas.add("23245");
		listContas.add("25436");
		listContas.add("78535");

	}

	@Test
	void test() {
		assertNotNull(mock.listAagencia());
		assertEquals(4, mock.listAagencia().size());

		assertNotNull(mock.listCcnpj());
		assertEquals(4, mock.listCcnpj().size());

		assertNotNull(mock.listCCpf());
		assertEquals(4, mock.listCCpf().size());
		
		assertNotNull(mock.listBbanco());
		assertEquals(4, mock.listBbanco().size());
		
		assertNotNull(mock.listCContas());
		assertEquals(4, mock.listCContas().size());
	}
	
	@Test
	void testNotEquals() {
		assertNotEquals(5, mock.listAagencia().size());
		assertNotEquals(3, mock.listCcnpj().size());
		assertNotEquals(6, mock.listCCpf().size());
		assertNotEquals(6, mock.listBbanco().size());
		assertNotEquals(5, mock.listCContas().size());
	}
	

}
